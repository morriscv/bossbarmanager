package network.marble.bossbarmanager.api;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;

import org.bukkit.entity.Player;
import org.inventivetalent.bossbar.BossBarAPI;

import net.md_5.bungee.api.chat.TextComponent;
import network.marble.bossbarmanager.BarRegisteredPlugin;
import network.marble.bossbarmanager.BossBarManager;
import network.marble.bossbarmanager.IdentifiablePluginString;

public class BossBarManagerAPI {
	
	public static boolean registerPlugin(String pluginName, int priority){
		if(BossBarManager.plugins.get(pluginName)==null){
			BarRegisteredPlugin newPlugin = new BarRegisteredPlugin(pluginName, priority);
			BossBarManager.plugins.put(pluginName, newPlugin);
			BossBarManager.pluginPlayers.put(newPlugin, new ArrayList<>());
			return true;
		}
		
		return false;
	}
	
	public static boolean unloadPlugin(String pluginName){
		updateStrings(pluginName, new ArrayList<>());
		
		Iterator<Entry<IdentifiablePluginString, BarRegisteredPlugin>> it = BossBarManager.pluginStrings.entrySet().iterator();
		while(it.hasNext()){
			Entry<IdentifiablePluginString, BarRegisteredPlugin> entry = it.next();
			if(entry.getValue().getPluginName().equals(pluginName)){
				it.remove();
			}
		}
		
		BossBarManager.plugins.remove(pluginName);
		
		return false;
	}
	
	public static boolean updateStrings(String pluginName, ArrayList<IdentifiablePluginString> strings){
		BarRegisteredPlugin updatingPlugin = BossBarManager.plugins.get(pluginName);
		if(updatingPlugin != null){
			//get all strings assigned to input plugin
			ArrayList<IdentifiablePluginString> existingStrings = new ArrayList<>();
			for (Entry<IdentifiablePluginString, BarRegisteredPlugin> entry : BossBarManager.pluginStrings.entrySet()) {
				if (entry.getValue().getPluginName().equals(pluginName)) {
					existingStrings.add(entry.getKey());
				}
			}
			
			ArrayList<IdentifiablePluginString> updates = new ArrayList<>();//keys that are to be updated

			for (IdentifiablePluginString newString : strings) {
				Iterator<IdentifiablePluginString> existingIterator = existingStrings.iterator();//remove old strings of same id as new ones
				while (existingIterator.hasNext()) {
					IdentifiablePluginString string = existingIterator.next();
					if (newString.getID() == string.getID()) {
						BossBarManager.pluginStrings.remove(string);
						existingIterator.remove();//removes from todo list of existing strings
						break;
					}
				}
				updates.add(newString);//prepares strings for addition
			}
			
			ArrayList<Player> playersToUpdate = BossBarManager.pluginPlayers.get(updatingPlugin);
			
			//insert new keys
			for (IdentifiablePluginString entry : updates) {
				BossBarManager.pluginStrings.put(entry, entry.getPluginString().getPlugin());

				for (Player playerToUpdate : playersToUpdate) {
					ArrayList<Entry<Player, IdentifiablePluginString>> stringsToBeAddedAfter = new ArrayList<>();

					Iterator<IdentifiablePluginString> PlayerStringsToUpdate = BossBarManager.playerStrings.get(playerToUpdate).iterator();
					while (PlayerStringsToUpdate.hasNext()) {
						IdentifiablePluginString playerStringToUpdate = PlayerStringsToUpdate.next();
						if (playerStringToUpdate.getID() == entry.getID() && playerStringToUpdate.getPluginString().getPlugin().getPluginName().equals(entry.getPluginString().getPlugin().getPluginName())) {
							stringsToBeAddedAfter.add(new AbstractMap.SimpleEntry<>(playerToUpdate, entry));
							PlayerStringsToUpdate.remove();
						}
					}

					Iterator<Entry<Player, IdentifiablePluginString>> stringsInAddQueueIterator = stringsToBeAddedAfter.iterator();
					while (stringsInAddQueueIterator.hasNext()) {
						Entry<Player, IdentifiablePluginString> playerStringToAdd = stringsInAddQueueIterator.next();
						BossBarManager.playerStrings.get(playerStringToAdd.getKey()).add(playerStringToAdd.getValue());
						stringsInAddQueueIterator.remove();
					}
				}
			}
			
			//remove no longer added keys
			for(IdentifiablePluginString ips : existingStrings){
				for (Player playerToUpdate : playersToUpdate) {
					Iterator<IdentifiablePluginString> PlayerStringsToUpdate = BossBarManager.playerStrings.get(playerToUpdate).iterator();
					while (PlayerStringsToUpdate.hasNext()) {
						IdentifiablePluginString playerStringToUpdate = PlayerStringsToUpdate.next();
						if (playerStringToUpdate.getID() == ips.getID() && playerStringToUpdate.getPluginString().getPlugin().getPluginName().equals(ips.getPluginString().getPlugin().getPluginName())) {
							PlayerStringsToUpdate.remove();
						}
					}
				}
				
				BossBarManager.pluginStrings.remove(ips);
			}
			renderPlayerBars(playersToUpdate);
			return true;
		}
		return false;
	}
	
	private static void renderPlayerBars(ArrayList<Player> playersToUpdate) {//TODO return players that had higher priority messages	
		for (Player playerToRender : playersToUpdate) {
			IdentifiablePluginString highestPriority = null;
			int priority = -1;
			
			for (IdentifiablePluginString playerStringToCheck : BossBarManager.playerStrings.get(playerToRender)) {
				int newPriority = playerStringToCheck.getPluginString().getPlugin().getPriority();
				if (newPriority > priority) {
					highestPriority = playerStringToCheck;
					priority = newPriority;
				}
			}
			BossBarAPI.removeAllBars(playerToRender);
			if (highestPriority != null) {
				BossBarAPI.addBar(playerToRender, new TextComponent(highestPriority.getPluginString().getString()), BossBarAPI.Color.GREEN, BossBarAPI.Style.PROGRESS, 1.0f);
			}

		}
	}

	public static boolean registerPlayers(ArrayList<Player> players, String pluginName, int MessageID){
		BarRegisteredPlugin updatingPlugin = BossBarManager.plugins.get(pluginName);
		if(updatingPlugin != null){
			IdentifiablePluginString ips = null;

			for (Entry<IdentifiablePluginString, BarRegisteredPlugin> messageToCheck : BossBarManager.pluginStrings.entrySet()) {
				if (messageToCheck.getValue().equals(updatingPlugin)) {
					if (messageToCheck.getKey().getID() == MessageID) {
						ips = messageToCheck.getKey();
					}
				}
			}
			
			
			
			if(ips!=null){
				for (Player playerToRender : players) {
					if (!BossBarManager.playerStrings.containsKey(playerToRender)) {
						BossBarManager.playerStrings.put(playerToRender, new ArrayList<>());
					}
					BossBarManager.playerStrings.get(playerToRender).add(ips);
					BossBarManager.pluginPlayers.get(updatingPlugin).add(playerToRender);
				}
				renderPlayerBars(players);
			}
		}
		return false;
	}
	
	public static void unregisterPlayers(ArrayList<Player> players, String pluginName){
		BarRegisteredPlugin updatingPlugin = BossBarManager.plugins.get(pluginName);
		
			Iterator<Player> playerCleanerIterator = players.iterator();
			while (playerCleanerIterator.hasNext()) {
				Player cleanerEntry = playerCleanerIterator.next();
				if(!BossBarManager.playerStrings.containsKey(cleanerEntry)){
					playerCleanerIterator.remove();
				}
			}
		
		
		if(updatingPlugin != null){
			for (Player playerToRemove : players) {
				Iterator<IdentifiablePluginString> stringsToRemoveIterator = BossBarManager.playerStrings.get(playerToRemove).iterator();
				while (stringsToRemoveIterator.hasNext()) {
					IdentifiablePluginString stringToRemove = stringsToRemoveIterator.next();
					if (stringToRemove.getPluginString().getPlugin().getPluginName().equals(pluginName)) {
						stringsToRemoveIterator.remove();
					}
				}
			}
			renderPlayerBars(players);
		}
	}
}
