package network.marble.bossbarmanager;

public class BarRegisteredPlugin {
	String pluginName;
	int priority;
	
	public BarRegisteredPlugin(String pluginName, int priority){
		this.pluginName = pluginName;
		this.priority = priority;
	}
	
	public String getPluginName(){
		return pluginName;
	}
	
	public int getPriority(){
		return priority;
	}
}
