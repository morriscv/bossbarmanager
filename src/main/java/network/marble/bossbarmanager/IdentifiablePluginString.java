package network.marble.bossbarmanager;

public class IdentifiablePluginString {
	
	int id;
	PluginString pluginString;
	
	public IdentifiablePluginString(int id, PluginString pluginString){
		this.id = id;
		this.pluginString = pluginString;
	}
	
	public PluginString getPluginString(){
		return pluginString;
	}
	
	public int getID(){
		return id;
	}
}
