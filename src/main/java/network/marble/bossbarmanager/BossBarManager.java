package network.marble.bossbarmanager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class BossBarManager extends JavaPlugin {
    private static BossBarManager plugin;
    
    
    public static Map<BarRegisteredPlugin, ArrayList<Player>> pluginPlayers = new HashMap<BarRegisteredPlugin, ArrayList<Player>>();
    public static Map<Player, List<IdentifiablePluginString>> playerStrings = new HashMap<Player, List<IdentifiablePluginString>>();
    
    public static Map<IdentifiablePluginString, BarRegisteredPlugin> pluginStrings = new HashMap<IdentifiablePluginString, BarRegisteredPlugin>();
    public static Map<String, BarRegisteredPlugin> plugins = new HashMap<String, BarRegisteredPlugin>();
    
    @Override
    public void onEnable() {
        plugin = this;
        getLogger().info("BossBarManager successfully loaded.");
    }
    
    public static BossBarManager getPlugin() {
        return plugin;
    }
}
