package network.marble.bossbarmanager;

public class PluginString {
	BarRegisteredPlugin plugin;
	String string;
	
	public PluginString(BarRegisteredPlugin plugin, String string){
		this.plugin = plugin;
		this.string = string;
	}
	
	public PluginString(String plugin, String string){
		this.plugin = BossBarManager.plugins.get(plugin);
		this.string = string;
	}
	
	public String getString(){
		return string;
	}
	
	public BarRegisteredPlugin getPlugin(){
		return plugin;
	}
}
